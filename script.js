const imageWrapper = document.querySelectorAll('.image-to-show');

let count = 0;
function showImages() {
  imageWrapper[count++].hidden = true;
  if (count === imageWrapper.length) count = 0;
  imageWrapper[count].hidden = false;
}

let start = setInterval(showImages, 3000);

const cancelBtn = document.createElement('button');
const resumeBtn = document.createElement('button');
cancelBtn.innerText = 'Прекратить';
resumeBtn.innerText = 'Возобновить показ';
document.body.append(cancelBtn);
document.body.append(resumeBtn);
cancelBtn.addEventListener('click', () => {
  clearInterval(start);
  start = false;
});

resumeBtn.addEventListener('click', () => {
  if (!start) start = setInterval(showImages, 3000);
});
